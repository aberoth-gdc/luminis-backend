"""
Helpful fixtures for unit tests.
"""

import typing
from typing import Generator

import pydantic
import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker
from sqlalchemy.pool import StaticPool

from app import crud, models, schemas
from app.api.deps import get_db
from app.db.base import Base
from app.main import app
from fastapi.testclient import TestClient
from tests.utils import random_email, random_lower_string

SQLALCHEMY_DATABASE_URL = "sqlite://"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL,
    connect_args={"check_same_thread": False},
    poolclass=StaticPool,
)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base.metadata.create_all(bind=engine)


def override_get_db():
    try:
        test_db = TestingSessionLocal()
        yield test_db
    finally:
        test_db.close()


app.dependency_overrides[get_db] = override_get_db


@pytest.fixture(scope="session")
def db() -> Generator:
    yield TestingSessionLocal()


@pytest.fixture(scope="module")
def client() -> Generator:
    with TestClient(app) as c:
        yield c


@pytest.fixture
def user(
    db: Session,
) -> typing.Generator[models.User, None, None]:
    user_ = crud.user.create(
        db,
        obj_in=schemas.UserCreate(
            username=random_lower_string(),
            email=random_email(),
            password=random_lower_string(),
        ),
    )
    yield user_
    crud.user.delete(db, id_=user_.id)
