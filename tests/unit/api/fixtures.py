"""
Fixtures for making API unit testing simpler.
"""

import typing

import pytest

from app import models
from app.core import security


@pytest.fixture
def user_token(user: models.User) -> typing.Generator[dict[str, str], None, None]:
    """
    Log in the test user and get some tokens.
    """
    yield {
        "Authorization": f"Bearer {security.create_access_token(user.id)}",
    }
