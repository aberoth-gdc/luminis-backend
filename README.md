# Basic Fastapi

## Backend Requirements

* [Docker](https://www.docker.com/).
* [Docker Compose](https://docs.docker.com/compose/install/).

## Backend local development

* Start the stack with Docker Compose:

```bash
docker-compose up -d
```

* Now you can open your browser and interact with these URLs:

Backend, JSON based web API based on OpenAPI: http://localhost/api/

Automatic interactive documentation with Swagger UI (from the OpenAPI backend):
http://localhost/docs

Alternative automatic documentation with ReDoc (from the OpenAPI backend):
http://localhost/redoc

PGAdmin, PostgreSQL web administration: http://localhost:5050

Flower, administration of Celery tasks: http://localhost:5555

Traefik UI, to see how the routes are being handled by the proxy:
http://localhost:8090

## Backend local development, additional details

### General workflow

By default, the dependencies are managed with
[Pipenv](https://pipenv.pypa.io/en/latest/). This can be installed by:

```bash
# Arch Linux
$ sudo pacman -S python-pipenv
# Debian
# Ubuntu
```

You can install all the dependencies with:

```console
$ pipenv install --dev
```

Then you can start a shell session with the new environment with:

```console
$ pipenv shell
```

Modify or add SQLAlchemy models in `app/models/`, Pydantic schemas in
`app/schemas/`, API endpoints in `app/api/`, CRUD (Create, Read,
Update, Delete) utils in `app/crud/`. The easiest might be to copy the
ones for Users (models, endpoints, and CRUD utils) and update them to your
needs.

Add and modify tasks to the Celery worker in `app/worker.py`.

If you need to install any additional package to the worker, add it to the file
`celeryworker.dockerfile`.

### Docker Compose Override

During development, you can change Docker Compose settings that will only affect
the local development environment, in the file `docker-compose.override.yml`.

The changes to that file only affect the local development environment, not the
production environment. So, you can add "temporary" changes that help the
development workflow.

For example, the directory with the backend code is mounted as a Docker "host
volume", mapping the code you change live to the directory inside the container.
That allows you to test your changes right away, without having to build the
Docker image again. It should only be done during development, for production,
you should build the Docker image with a recent version of the backend code. But
during development, it allows you to iterate very fast.

There is also a command override that runs `/start-reload.sh` (included in the
base image) instead of the default `/start.sh` (also included in the base
image). It starts a single server process (instead of multiple, as would be for
production) and reloads the process whenever the code changes. Have in mind that
if you have a syntax error and save the Python file, it will break and exit, and
the container will stop. After that, you can restart the container by fixing the
error and running again:

```console
$ docker-compose up -d
```

There is also a commented out `command` override, you can uncomment it and
comment the default one. It makes the backend container run a process that does
"nothing", but keeps the container alive. That allows you to get inside your
running container and execute commands inside, for example a Python interpreter
to test installed dependencies, or start the development server that reloads
when it detects changes, or start a Jupyter Notebook session.

To get inside the container with a `bash` session you can start the stack with:

```console
$ docker-compose up -d
```

and then `exec` inside the running container:

```console
$ docker-compose exec backend bash
```

You should see an output like:

```console
root@7f2607af31c3:/app#
```

that means that you are in a `bash` session inside your container, as a `root`
user, under the `/app` directory.

There you can use the script `/start-reload.sh` to run the debug live reloading
server. You can run that script from inside the container with:

```console
$ bash /start-reload.sh
```

...it will look like:

```console
root@7f2607af31c3:/app# bash /start-reload.sh
```

and then hit enter. That runs the live reloading server that auto reloads when
it detects code changes.

Nevertheless, if it doesn't detect a change but a syntax error, it will just
stop with an error. But as the container is still alive and you are in a Bash
session, you can quickly restart it after fixing the error, running the same
command ("up arrow" and "Enter").

...this previous detail is what makes it useful to have the container alive
doing nothing and then, in a Bash session, make it run the live reload server.

### Backend tests

To test the backend run:

```console
$ ./do test
```

The tests run with Pytest, modify and add tests to `tests/unit`.

If you use GitLab CI the tests will run automatically.

#### Test Coverage

Because the test scripts forward arguments to `pytest`, you can enable test
coverage HTML report generation by passing `--cov-report=html`.

### Migrations

As during local development your app directory is mounted as a volume inside the
container, you can also run the migrations with `alembic` commands inside the
container and the migration code will be in your app directory (instead of being
only inside the container). So you can add it to your git repository.

Make sure you create a "revision" of your models and that you "upgrade" your
database with that revision every time you change them. As this is what will
update the tables in your database. Otherwise, your application will have
errors.

* Start an interactive session in the backend container:

```console
$ docker-compose exec backend bash
```

* If you created a new model in `app/models/`, make sure to import it in
   `app/db/base.py`, that Python module (`base.py`) that imports all the models
   will be used by Alembic.

* After changing a model (for example, adding a column), inside the container,
   create a revision, e.g.:

```console
$ alembic revision --autogenerate -m "Add column last_name to User model"
```

* Commit to the git repository the files generated in the alembic directory.

* After creating the revision, run the migration in the database (this is what
   will actually change the database):

```console
$ alembic upgrade head
```

If you don't want to use migrations at all, uncomment the line in the file at
`app/db/init_db.py` with:

```python
Base.metadata.create_all(bind=engine)
```

and comment the line in the file `prestart.sh` that contains:

```console
$ alembic upgrade head
```

If you don't want to start with the default models and want to remove them /
modify them, from the beginning, without having any previous revision, you can
remove the revision files (`.py` Python files) under `app/alembic/versions/`.
And then create a first migration as described above.

### Authentication

Authentication for this application is handled through [jwt](https://jwt.io/)s.
The jwt encodes the subject to authenticate as (the users id) and the expiration
date of the token. To require login for a path add the `get_current_user`
dependency. This will check headers for the bearer authentication field for a
jwt and decode it to verify the identity of the requestor. Jwts are issued
through the login path, `/login/access-token`, and through qrcode authentication
with the websocket route `/qr-auth/ws/{session_id}`.

## Deployment

You can deploy the stack to a Docker Swarm mode cluster with a main Traefik
proxy, set up using the ideas from <a href="https://dockerswarm.rocks"
target="_blank">DockerSwarm.rocks</a>, to get automatic HTTPS certificates, etc.

And you can use CI (continuous integration) systems to do it automatically.

But you have to configure a couple things first.

### Traefik network

This stack expects the public Traefik network to be named `traefik-public`, just
as in the tutorials in <a href="https://dockerswarm.rocks" class="external-link"
target="_blank">DockerSwarm.rocks</a>.

If you need to use a different Traefik public network name, update it in the
`docker-compose.yml` files, in the section:

```YAML
networks:
  traefik-public:
    external: true
```

Change `traefik-public` to the name of the used Traefik network. And then update
it in the file `.env`:

```bash
TRAEFIK_PUBLIC_NETWORK=traefik-public
```

### Persisting Docker named volumes

You need to make sure that each service (Docker container) that uses a volume is
always deployed to the same Docker "node" in the cluster, that way it will
preserve the data. Otherwise, it could be deployed to a different node each
time, and each time the volume would be created in that new node before starting
the service. As a result, it would look like your service was starting from
scratch every time, losing all the previous data.

That's specially important for a service running a database. But the same
problem would apply if you were saving files in your main backend service (for
example, if those files were uploaded by your users, or if they were created by
your system).

To solve that, you can put constraints in the services that use one or more data
volumes (like databases) to make them be deployed to a Docker node with a
specific label. And of course, you need to have that label assigned to one (only
one) of your nodes.

### Deploy to a Docker Swarm mode cluster

There are 3 steps:

1. **Build** your app images
2. Optionally, **push** your custom images to a Docker Registry
3. **Deploy** your stack

---

Here are the steps in detail:

1. **Build your app images**

* Set these environment variables, right before the next command:
  * `TAG=prod`
  * `FRONTEND_ENV=production`
* Use the provided `scripts/build.sh` file with those environment variables:

```bash
TAG=prod FRONTEND_ENV=production bash scripts/build.sh
```

2. **Optionally, push your images to a Docker Registry**

**Note**: if the deployment Docker Swarm mode "cluster" has more than one
server, you will have to push the images to a registry or build the images in
each server, so that when each of the servers in your cluster tries to start the
containers it can get the Docker images for them, pulling them from a Docker
Registry or because it has them already built locally.

If you are using a registry and pushing your images, you can omit running the
previous script and instead using this one, in a single shot.

* Set these environment variables:
  * `TAG=prod`
  * `FRONTEND_ENV=production`
* Use the provided `scripts/build-push.sh` file with those environment
   variables:

```bash
TAG=prod FRONTEND_ENV=production bash scripts/build-push.sh
```

3. **Deploy your stack**

* Set these environment variables:
  * `DOMAIN=<website>.com`
  * `TRAEFIK_TAG=<website>.com`
  * `STACK_NAME=<website>-com`
  * `TAG=prod`
* Use the provided `scripts/deploy.sh` file with those environment variables:

```bash
DOMAIN=<website>.com \
TRAEFIK_TAG=<website>.com \
STACK_NAME=<website>-com \
TAG=prod \
bash scripts/deploy.sh
```

---

If you change your mind and, for example, want to deploy everything to a
different domain, you only have to change the `DOMAIN` environment variable in
the previous commands. If you wanted to add a different version / environment of
your stack, like "`preproduction`", you would only have to set
`TAG=preproduction` in your command and update these other environment variables
accordingly. And it would all work, that way you could have different
environments and deployments of the same app in the same cluster.

#### Deployment Technical Details

Building and pushing is done with the `docker-compose.yml` file, using the
`docker-compose` command. The file `docker-compose.yml` uses the file `.env`
with default environment variables. And the scripts set some additional
environment variables as well.

The deployment requires using `docker stack` instead of `docker-swarm`, and it
can't read environment variables or `.env` files. Because of that, the
`deploy.sh` script generates a file `docker-stack.yml` with the configurations
from `docker-compose.yml` and injecting the environment variables in it. And
then uses it to deploy the stack.

You can do the process by hand based on those same scripts if you wanted. The
general structure is like this:

```bash
# Use the environment variables passed to this script, as TAG and FRONTEND_ENV
# And re-create those variables as environment variables for the next command
TAG=${TAG} \
# Set the environment variable FRONTEND_ENV to the same value passed to this
# script with a default value of "production" if nothing else was passed
FRONTEND_ENV=${FRONTEND_ENV-production} \
# The actual comand that does the work: docker-compose
docker-compose \
# Pass the file that should be used, setting explicitly docker-compose.yml
# avoids the default of also using docker-compose.override.yml
-f docker-compose.yml \
# Use the docker-compose sub command named "config", it just uses the
# docker-compose.yml file passed to it and prints their combined contents Put
# those contents in a file "docker-stack.yml", with ">"
config > docker-stack.yml

# The previous only generated a docker-stack.yml file,
# but didn't do anything with it yet

# docker-auto-labels makes sure the labels used for constraints exist in the
# cluster
docker-auto-labels docker-stack.yml

# Now this command uses that same file to deploy it
docker stack deploy -c docker-stack.yml --with-registry-auth "${STACK_NAME}"
```

### Continuous Integration / Continuous Delivery

If you use GitLab CI, the included `.gitlab-ci.yml` can automatically deploy it.
You may need to update it according to your GitLab configurations.

If you use any other CI / CD provider, you can base your deployment from that
`.gitlab-ci.yml` file, as all the actual script steps are performed in `bash`
scripts that you can easily re-use.

GitLab CI is configured assuming 2 environments following GitLab flow:

* `prod` (production) from the `production` branch.
* `stag` (staging) from the `master` branch.

If you need to add more environments, for example, you could imagine using a
client-approved `preprod` branch, you can just copy the configurations in
`.gitlab-ci.yml` for `stag` and rename the corresponding variables. The Docker
Compose file and environment variables are configured to support as many
environments as you need, so that you only need to modify `.gitlab-ci.yml` (or
whichever CI system configuration you are using).

## Docker Compose files and env vars

There is a main `docker-compose.yml` file with all the configurations that apply
to the whole stack, it is used automatically by `docker-compose`.

And there's also a `docker-compose.override.yml` with overrides for development,
for example to mount the source code as a volume. It is used automatically by
`docker-compose` to apply overrides on top of `docker-compose.yml`.

These Docker Compose files use the `.env` file containing configurations to be
injected as environment variables in the containers.

They also use some additional configurations taken from environment variables
set in the scripts before calling the `docker-compose` command.

It is all designed to support several "stages", like development, building,
testing, and deployment. Also, allowing the deployment to different environments
like staging and production (and you can add more environments very easily).

They are designed to have the minimum repetition of code and configurations, so
that if you need to change something, you have to change it in the minimum
amount of places. That's why files use environment variables that get
auto-expanded. That way, if for example, you want to use a different domain, you
can call the `docker-compose` command with a different `DOMAIN` environment
variable instead of having to change the domain in several places inside the
Docker Compose files.

Also, if you want to have another deployment environment, say `preprod`, you
just have to change environment variables, but you can keep using the same
Docker Compose files.

### The .env file

The `.env` file is the one that contains all your configurations, generated keys
and passwords, etc.

Depending on your workflow, you could want to exclude it from Git, for example
if your project is public. In that case, you would have to make sure to set up a
way for your CI tools to obtain it while building or deploying your project.

One way to do it could be to add each environment variable to your CI/CD system,
and updating the `docker-compose.yml` file to read that specific env var instead
of reading the `.env` file.

## URLs

### Production URLs



### Staging URLs



### Development URLs

Development URLs, for local development.

Frontend: http://localhost

Backend: http://localhost/api/

Automatic Interactive Docs (Swagger UI): https://localhost/docs

Automatic Alternative Docs (ReDoc): https://localhost/redoc

PGAdmin: http://localhost:5050

Flower: http://localhost:5555

Traefik UI: http://localhost:8090
