#! /usr/bin/env sh
set -ex

# Let the DB start
python "${APP_HOME}/app/backend_pre_start.py"

# Run migrations
alembic upgrade head

# Create initial data in DB
python "${APP_HOME}/app/initial_data.py"

APP_MODULE="app.main:app"
GUNICORN_CONF="${APP_HOME}/gunicorn_conf.py"
WORKER_CLASS=uvicorn.workers.UvicornWorker

if [[ "${DEBUG}" = "true" ]]; then
    # Start Uvicorn with live reload
    echo "Running uvicorn with live reload"
    exec uvicorn --reload --reload-dir ${APP_HOME}/app --host 0.0.0.0 --port 80 --log-level debug "$APP_MODULE"
else
    # Start Gunicorn
    echo "Running gunicorn"
    exec gunicorn -k "${WORKER_CLASS}" -c "${GUNICORN_CONF}" "${APP_MODULE}"
fi
