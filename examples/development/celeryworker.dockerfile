###########
# Builder #
###########

FROM python:3.12-alpine as builder

LABEL maintainer="Conceptual"

# Dev runtime
RUN apk add --no-cache --virtual .build-deps build-base python3-dev

# Install the installers. Keep these updated with latest
RUN pip install --upgrade pip==23.1.2
RUN pip install --upgrade setuptools==67.8.0
RUN pip install --upgrade pipenv==2023.5.19

# Don't create pyc
ENV PYTHONDONTWRITEBYTECODE 1
# Don't buffer outputs (recommended for python in docker)
ENV PYTHONUNBUFFERED 1

WORKDIR /usr/src/app

# install psycopg2 dependencies
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev libffi-dev

# Create wheels for production
COPY Pipfile Pipfile.lock ./
RUN set -ex
ENV HOME=/tmp

ARG INSTALL_DEV=false
RUN if [ $INSTALL_DEV = "true" ] ; then pipenv requirements --dev  > requirements.txt ; else pipenv requirements  > requirements.txt ; fi

RUN pip wheel --no-cache-dir --no-deps --wheel-dir ./wheels -r requirements.txt


########
# Main #
########

FROM python:3.12-alpine AS main

# Create directory for the app user
ENV APP_HOME=/home/app/app
RUN mkdir -p $APP_HOME

WORKDIR $APP_HOME

# Create the app user
RUN addgroup -S app && adduser -S app -G app

# Install dependencies
RUN apk update && apk add libpq
COPY --from=builder /usr/src/app/wheels ./wheels
COPY --from=builder /usr/src/app/requirements.txt .
RUN pip install --no-cache ./wheels/*

# Copy project
ENV PYTHONPATH=/home/app
COPY ./app ./app

# For development, Jupyter remote kernel, Hydrogen
# Using inside the container:
# jupyter lab --ip=0.0.0.0 --allow-root --NotebookApp.custom_display_url=http://127.0.0.1:8888
ARG INSTALL_JUPYTER=false
RUN if [ $INSTALL_JUPYTER = "true" ] ; then pip install jupyterlab ; fi

# Configure gunicorn
COPY ./gunicorn_conf.py ./gunicorn_conf.py

# Copy starter scripts
COPY ./scripts/build/worker-start.sh ./scripts/build/worker-start.sh
RUN chmod +x ./scripts/build/worker-start.sh

# Chown all the files to the app user
RUN chown -R app:app $APP_HOME

# Change to the app user
USER app

ENV PYTHONPATH=$APP_HOME

# Force celery to use root
ENV C_FORCE_ROOT=1

CMD ["./scripts/build/worker-start.sh"]
