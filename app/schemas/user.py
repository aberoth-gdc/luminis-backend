"""Describes a user."""

from pydantic import BaseModel, ConfigDict, EmailStr


class UserBase(BaseModel):
    """Shared user properties."""


class UserCreate(UserBase):
    """Properties to receive via API on creation."""

    username: str
    email: EmailStr
    access_token: str
    refresh_token: str


class UserUpdate(UserBase):
    """Properties to receive via API on update."""

    access_token: str
    refresh_token: str


class UserProfile(UserBase):
    """
    Discord stuff
    """

    username: str
    avatar: str


class UserInDBBase(UserBase):
    """Describes user in db."""

    model_config = ConfigDict(from_attributes=True)

    id: int | None = None


class User(UserInDBBase):
    """Additional properties to return via API."""


class UserInDB(UserInDBBase):
    """Additional properties stored in DB."""

    hashed_password: str
