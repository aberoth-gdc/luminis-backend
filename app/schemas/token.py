"""Describes token payloads."""

from pydantic import BaseModel


class Token(BaseModel):
    """How the api reads authentication tokens."""

    access_token: str
    token_type: str


class TokenPayload(BaseModel):
    """Describes the data encoded into our JWTs."""

    sub: int | None = None
