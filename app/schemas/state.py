"""
Lmao
"""
from pydantic import BaseModel


class DiscordCode(BaseModel):
    code: str


class Boss(BaseModel):
    name: str
    channel: str
    last_killed: int
    min_spawn: int
    max_spawn: int
    camped_by: str


class Realm(BaseModel):
    name: str
    bosses: dict[str, Boss]


class GameState(BaseModel):
    realms: dict[str, Realm]


class Mark(BaseModel):
    realm: str
    name: str
    last_killed: int
