"""
Initializaes schemas.
https://pydantic-docs.helpmanual.io/usage/models/
"""

from .msg import Msg
from .state import Boss, GameState, Mark, Realm
from .token import Token, TokenPayload
from .user import User, UserCreate, UserInDB, UserProfile, UserUpdate
