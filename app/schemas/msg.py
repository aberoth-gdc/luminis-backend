"""Describes a message."""

from pydantic import BaseModel


class Msg(BaseModel):
    """A message from the api."""

    msg: str
