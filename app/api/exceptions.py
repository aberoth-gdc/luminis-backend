"""
Exceptions for common return cases.
"""

from fastapi import HTTPException


class InsufficientPrivilegesException(HTTPException):
    """
    Reports that a request was made that the logged in User cannot perform.
    """

    def __init__(self) -> None:
        super().__init__(status_code=403, detail="Insufficient permissions.")


class UserNotFoundException(HTTPException):
    """
    Reports that a requested User does not exist.
    """

    def __init__(self) -> None:
        super().__init__(status_code=404, detail="User not found.")
