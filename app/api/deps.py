"""
Contains all dependencies for the fastapi app.

https://fastapi.tiangolo.com/tutorial/dependencies/
"""

import logging
import pickle
from typing import Generator

import redis
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import jwt
from pydantic import ValidationError
from redis import Redis
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api.exceptions import UserNotFoundException
from app.core import security
from app.core.settings import settings
from app.db.session import SessionLocal

logger: logging.Logger = logging.getLogger(__name__)

reusable_oauth2 = OAuth2PasswordBearer(
    tokenUrl=f"{settings.API_V1_STR}/login/access-token"
)


def get_db() -> Generator:
    """Yield a db connection."""
    logger.debug(f"Connecting to database {str(settings.POSTGRES.DSN)}")
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


def get_cache() -> Generator:
    """Yield a cache connection."""
    logger.debug(f"Connecting to cache {str(settings.REDIS.DSN)}")
    try:
        cache = redis.from_url(str(settings.REDIS.DSN))
        yield cache
    finally:
        cache.close()


def get_current_user(
    db: Session = Depends(get_db), token: str = Depends(reusable_oauth2)
) -> models.User:
    """Returns a user via the bearer token auth."""
    try:
        logger.info("Verifying")
        payload = jwt.decode(
            token, settings.SECRET_KEY, algorithms=[security.ALGORITHM]
        )
        token_data = schemas.TokenPayload(**payload)
    except (jwt.JWTError, ValidationError) as exc:
        detail = "Could not validate credentials"
        logging.error(detail)
        logging.error(exc)
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=detail,
        ) from exc
    user = crud.user.read(db, id_=token_data.sub)
    if not user:
        detail = "User not found"
        logger.error(detail)
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=detail)
    return user


def get_current_active_user(
    current_user: models.User = Depends(get_current_user),
) -> models.User:
    """Returns current user if they are active."""
    if not crud.user.is_active(current_user):
        logger.error("Inactive user")
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Inactive user"
        )
    return current_user


def get_current_active_superuser(
    current_user: models.User = Depends(get_current_user),
) -> models.User:
    """Returns current user if they are both active and a superuser."""
    if not crud.user.is_superuser(current_user):
        detail = "The user doesn't have enough privileges"
        logger.error(detail)
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=detail)
    return current_user


def user(
    user_id: int,
    db: Session = Depends(get_db),
) -> models.User:
    """
    Returns the User from the database.

    Raises a 404 if it doesn't exist.
    """
    user: models.User | None = crud.user.read(
        db,
        id_=user_id,
    )

    if user is None:
        raise UserNotFoundException

    return user


def boss(
    name: str,
    realm: str,
    cache: Redis = Depends(get_cache),
) -> schemas.Boss:
    ret = cache.get(f"{realm}/{name}")
    if ret is None:
        logger.info("Could not find thing")
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST)
    return pickle.loads(ret)
