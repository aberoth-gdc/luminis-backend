"""Adds all routes to FastAPI."""


from fastapi import APIRouter

from .endpoints import login, luminis

router = APIRouter()
router.include_router(login.router, tags=["login"])
router.include_router(luminis.router, tags=["luminis"])
