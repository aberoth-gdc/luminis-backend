"""
Endpoints for keeping track of boss mark/camp state in Aberoth
"""

import logging
import pickle
import time

import requests
from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    WebSocket,
    WebSocketDisconnect,
)
from redis import Redis

from app import models, schemas
from app.api.deps import boss, get_cache, get_current_user
from app.core.settings import settings
from app.resources.aberoth import bosses, realms, webhooks

router = APIRouter()

logger: logging.Logger = logging.getLogger(__name__)


class ConnectionManager:
    """
    Adapted from https://fastapi.tiangolo.com/advanced/websockets/

    Stores / manages a dictionary that indexes websocket connections with
    session ids determined by the subscriber. This allows a web app to initiate
    login "sessions" that can be authenticated by a seperate logged in app (ie
    a mobile app).
    """

    def __init__(self):
        self.active_connections: dict[str, list[WebSocket]] = {}

    async def connect(self, id_: str, websocket: WebSocket):
        """Connect a new websocket and save it."""
        await websocket.accept()
        if id_ not in self.active_connections:
            self.active_connections[id_] = []
        self.active_connections[id_].append(websocket)

    def disconnect(self, id_: str):
        """Disconnect and remove a websocket."""
        if id_ in self.active_connections:
            logger.debug(f"Disconnecting {id_}")
            del self.active_connections[id_]

    async def send_personal_message(self, message: str, id_: str):
        """Send a message over a specific socket."""
        websockets = self.active_connections.get(id_)
        if not websockets:
            raise HTTPException(
                status_code=404,
                detail=f"No session with id {id_} found.",
            )
        for websocket in websockets:
            await websocket.send_text(message)

    async def broadcast(self, message: str):
        """Send a message to all active sockets."""
        logger.info(f"Broadcasting message {message}")
        for websockets in self.active_connections.values():
            for websocket in websockets:
                await websocket.send_text(message)


manager = ConnectionManager()


@router.get("/state")
async def read_state(
    current_user: models.User = Depends(get_current_user),
    cache: Redis = Depends(get_cache),
) -> None:
    """
    Mark a boss in a realm as recently killed
    """
    # ret = {}
    ret = schemas.GameState(realms={})
    for realm in realms:
        r = schemas.Realm(
            name=realm,
            bosses={},
        )
        for b in bosses:
            data = cache.get(f"{realm}/{b[0]}")
            if data:
                r.bosses[b[0]] = pickle.loads(data)
        ret.realms[realm] = r

    return ret


@router.post("/mark")
async def mark(
    realm: str,
    b: schemas.Boss = Depends(boss),
    current_user: models.User = Depends(get_current_user),
    cache: Redis = Depends(get_cache),
) -> None:
    """
    Mark a boss in a realm as recently killed
    """
    logger.info("Marking")
    if (int(time.time()) - b.last_killed) < b.min_spawn:
        return

    b.last_killed = int(time.time())
    cache.set(f"{realm}/{b.name}", pickle.dumps(b))

    await manager.broadcast(
        schemas.Mark(
            realm=realm,
            name=b.name,
            last_killed=b.last_killed,
        ).model_dump_json()
    )

    # resp: requests.Response = requests.post(
    #    b.channel,
    #    data=json.dumps(
    #        {
    #            "content": f"** {b.name} ** was killed by <@{current_user.username}>",
    #            "allowed_mentions": {"parse": []},
    #        }
    #    ),
    #    headers={
    #        "Content-Type": "application/json",
    #    },
    #    timeout=60,
    # )
    # if resp.status_code != 204:
    #    logger.error(resp.content)


@router.post("/clear")
async def clear(
    cache: Redis = Depends(get_cache),
) -> None:
    """
    Clears the state.
    """
    for realm in realms:
        for b in bosses:
            data = cache.get(f"{realm}/{b[0]}")
            if data:
                newBoss = pickle.loads(data)
                newBoss.last_killed = 0
            else:
                newBoss = schemas.Boss(
                    name=b[0],
                    channel=webhooks[f"{realm}/{b[0]}"],
                    last_killed=0,
                    min_spawn=b[1],
                    max_spawn=b[2],
                    camped_by="",
                )
            cache.set(f"{realm}/{b[0]}", pickle.dumps(newBoss))
            logger.info(f"Resetting {realm}/{b[0]}")


@router.post("/camp")
async def camp(
    b: schemas.Boss = Depends(boss),
    current_user: models.User = Depends(get_current_user),
    cache: Redis = Depends(get_cache),
) -> None:
    """
    Mark a boss in a realm as being camped.
    """
    if b.camped_by == "":
        b.camped_by = current_user.username
        cache.set(f"{b.realm}/{b.name}", pickle.dumps(b))


@router.get("/user")
async def user(
    current_user: models.User = Depends(get_current_user),
) -> schemas.UserProfile:
    """
    Get the User Profile
    """
    headers = {
        "Authorization": f"Bearer {current_user.access_token}",
    }
    resp = requests.get(f"{settings.DISCORD.URL}/users/@me", headers=headers)
    if resp.status_code == 403:
        return HTTPException(status_code=403)

    logger.info(resp.json())

    username: str = ""
    avatar: str = ""

    if "username" in resp.json():
        username = resp.json()["username"]

    if "avatar" in resp.json():
        avatar = f"{settings.DISCORD.CDN}/avatars/{current_user.username}/{resp.json()['avatar']}"
    else:
        avatar = f"{settings.DISCORD.CDN}/embed/avatars/{int(current_user.username) >> 22 % 6}.png"

    return schemas.UserProfile(
        username=username,
        avatar=avatar,
    )


@router.websocket("/ws/{discord_id}")
async def websocket_endpoint(websocket: WebSocket, discord_id: str):
    """
    Initiates qr auth websocket. Hold the socket open until the subscriber
    disconnects or sends 'close'
    """
    await manager.connect(discord_id, websocket)
    try:
        while True:
            data = await websocket.receive_text()
            if data == "close":
                raise WebSocketDisconnect
    except WebSocketDisconnect:
        manager.disconnect(discord_id)
