import logging
from datetime import timedelta
from typing import Any

import requests
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api.deps import get_db
from app.core import security
from app.core.metrics import REQUEST_TIME
from app.core.settings import settings

router = APIRouter()

logger: logging.Logger = logging.getLogger(__name__)


def get_discord_token(code: str) -> tuple[str, str]:
    """
    First step of Discord OAuth
    """
    headers = {
        "Authorization": f"Bearer {code}",
    }
    logger.info(f"Redirect URI: {settings.REDIRECT_URI}")
    data = {
        "grant_type": "authorization_code",
        "code": code,
        "redirect_uri": settings.REDIRECT_URI,
    }
    headers = {"Content-Type": "application/x-www-form-urlencoded"}

    resp = requests.post(
        f"{settings.DISCORD.URL}/oauth2/token",
        headers=headers,
        data=data,
        auth=(settings.DISCORD.ID, settings.DISCORD.SECRET),
    )
    if resp.status_code != 200:
        logger.error(resp.json())

    return resp.json()["access_token"], resp.json()["refresh_token"]


@REQUEST_TIME.time()
@router.post("/login/discord", status_code=201)
async def discord_oauth(
    code: str,
    db: Session = Depends(get_db),
) -> Any:
    """
    Discord OAUTH Redirect
    """
    logger.info(f"Discord OAUTH Code: {code}")

    access_token, refresh_token = get_discord_token(code)

    headers = {
        "Authorization": f"Bearer {access_token}",
    }
    resp = requests.get(f"{settings.DISCORD.URL}/users/@me", headers=headers)
    user: models.User | None = crud.user.get_by_username(db, username=resp.json()["id"])
    if user is None:
        user = crud.user.create(
            db,
            obj_in=schemas.UserCreate(
                email=resp.json()["email"],
                username=resp.json()["id"],
                access_token=access_token,
                refresh_token=refresh_token,
            ),
        )
    else:
        crud.user.update(
            db,
            db_obj=user,
            obj_in=schemas.UserUpdate(
                access_token=access_token,
                refresh_token=refresh_token,
            ),
        )
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    return {
        "access_token": security.create_access_token(
            user.id, expires_delta=access_token_expires
        ),
        "token_type": "bearer",
    }
