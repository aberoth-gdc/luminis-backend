"""Sets the urls for user endpoints."""

import logging

from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps
from app.core.metrics import REQUEST_TIME
from app.core.settings import settings
from app.utils import send_new_account_email

router = APIRouter()

logger: logging.Logger = logging.getLogger(__name__)


@REQUEST_TIME.time()
@router.post(
    "/",
    status_code=201,
    responses={
        201: {"description": "User created.", "model": schemas.User},
        400: {"description": "The user already exists."},
    },
    operation_id="create_user",
)
async def create_user(
    *,
    db: Session = Depends(deps.get_db),
    user_in: schemas.UserCreate,
    # current_user: models.User = Depends(deps.get_current_active_superuser),
) -> schemas.User:
    """
    Create new user.
    """
    user: models.User | None = crud.user.get_by_email(db, email=user_in.email)
    if user is not None:
        logger.error("Could not create user {user_in.email}: Already exists.")
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="The user with this username already exists in the system.",
        )
    user = crud.user.create(db, obj_in=user_in)
    # Email might not be configured, so catch AttributeError
    try:
        if settings.email.ENABLED and user_in.email:
            send_new_account_email(
                email_to=user_in.email,
                username=user_in.email,
                password=user_in.password,
            )

    except AttributeError:
        pass

    return user


@REQUEST_TIME.time()
@router.get(
    "/me",
    responses={
        "200": {"model": schemas.User},
        "403": {},
    },
    operation_id="get_me",
)
async def read_user_me(
    current_user: models.User = Depends(deps.get_current_user),
) -> schemas.User:
    """
    Get current user.
    """
    return current_user


@REQUEST_TIME.time()
@router.get(
    "/",
    responses={
        200: {"description": "", "model": list[schemas.User]},
        400: {"description": "The user doesn't have enough privileges."},
    },
    operation_id="read_users",
)
async def read_users(
    db: Session = Depends(deps.get_db),
    skip: int = 0,
    limit: int = 100,
    # current_user: models.User = Depends(deps.get_current_active_superuser),
) -> schemas.User:
    """
    Retrieve users.
    """
    users = crud.user.get_multi(db, skip=skip, limit=limit)
    return users


@REQUEST_TIME.time()
@router.get(
    "/{user_id}",
    responses={
        200: {"description": "", "model": schemas.User},
        401: {"description": "The user doesn't have enough privileges."},
    },
    operation_id="read_user",
)
async def read_user(
    user: models.User = Depends(deps.user),
    current_user: models.User = Depends(deps.get_current_user),
) -> schemas.User:
    """
    Get a specific user by id.
    """
    if user == current_user:
        return user
    if not crud.user.is_superuser(current_user):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="The user doesn't have enough privileges",
        )

    return user


@REQUEST_TIME.time()
@router.put(
    "/me",
    responses={
        "200": {"model": schemas.User},
        "403": {},
    },
    operation_id="update_me",
)
async def update_user_me(
    *,
    db: Session = Depends(deps.get_db),
    user_in: schemas.UserUpdate,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> schemas.User:
    """
    Update own user.
    """
    user = crud.user.update(db, db_obj=current_user, obj_in=user_in)
    return user


@REQUEST_TIME.time()
@router.put(
    "/{user_id}",
    responses={
        200: {"description": "", "model": schemas.User},
        403: {"description": "The user doesn't have enough privileges."},
    },
    operation_id="update_user",
)
async def update_user(
    *,
    db: Session = Depends(deps.get_db),
    user: models.User = Depends(deps.user),
    user_in: schemas.UserUpdate,
    current_user: models.User = Depends(deps.get_current_user),
) -> schemas.User:
    """
    Update a user.
    """
    if user != current_user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
        )
    user = crud.user.update(db, db_obj=user, obj_in=user_in)
    return user


@REQUEST_TIME.time()
@router.delete(
    "/{user_id}",
    responses={
        "200": {},
        "403": {},
    },
    operation_id="delete_user",
)
async def delete_user(
    *,
    db: Session = Depends(deps.get_db),
    user: models.User = Depends(deps.user),
    current_user: models.User = Depends(deps.get_current_active_user),
) -> None:
    """
    Delete a user.
    """
    if user != current_user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
        )

    crud.user.delete(db, id_=user.id)
