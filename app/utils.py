"""Utility functions for the application."""

import logging
from datetime import datetime, timedelta
from pathlib import Path
from time import time
from typing import Any, Dict, Optional

import emails
from emails.template import JinjaTemplate
from jose import jwt

from app.core.settings import settings

logger: logging.Logger = logging.getLogger(__name__)


def send_email(
    email_to: str,
    subject_template: str = "",
    html_template: str = "",
    environment: Optional[Dict[str, Any]] = None,
) -> None:
    """Send an email using a jinja template."""
    if environment is None:
        environment = {}

    if settings.EMAILS_ENABLED:
        message = emails.Message(
            subject=JinjaTemplate(subject_template),
            html=JinjaTemplate(html_template),
            mail_from=(settings.EMAILS_FROM_NAME, settings.EMAILS_FROM_EMAIL),
        )
        smtp_options = {"host": settings.SMTP_HOST, "port": settings.SMTP_PORT}
        if settings.SMTP_TLS:
            smtp_options["tls"] = True
        if settings.SMTP_USER:
            smtp_options["user"] = settings.SMTP_USER
        if settings.SMTP_PASSWORD:
            smtp_options["password"] = settings.SMTP_PASSWORD
        response = message.send(to=email_to, render=environment, smtp=smtp_options)
        logging.info("send email result: %s", response)
    else:
        message = emails.Message(
            subject=JinjaTemplate(subject_template),
            html=JinjaTemplate(html_template),
            mail_from=(settings.EMAILS_FROM_NAME, settings.EMAILS_FROM_EMAIL),
        )
        email_filename = f"sent_on_{int(time())}"
        logging.info("email saved to: %s", email_filename)
        with open(
            f"{settings.EMAIL_LOCAL_SENT_DIR}/email_{email_filename}.html",
            "w",
            encoding="utf-8",
        ) as file:
            message.transform()
            file.write(message.html.render(**environment))


def send_test_email(email_to: str) -> None:
    """Sends a test email."""
    project_name: str = settings.PROJECT_NAME
    subject: str = f"{project_name} - Test email"
    with open(
        Path(settings.EMAIL_TEMPLATES_DIR) / "test_email.html", encoding="utf-8"
    ) as file:
        template_str = file.read()
    send_email(
        email_to=email_to,
        subject_template=subject,
        html_template=template_str,
        environment={"project_name": project_name, "email": email_to},
    )


def send_reset_password_email(email_to: str, email: str, token: str) -> None:
    """Sends an email using the reset password template."""
    project_name: str = settings.PROJECT_NAME
    subject: str = f"{project_name} - Password recovery for user {email}"
    with open(
        Path(settings.EMAIL_TEMPLATES_DIR) / "reset_password.html", encoding="utf-8"
    ) as file:
        template_str = file.read()
    server_host = settings.SERVER_HOST
    link = f"{server_host}/reset-password?token={token}"
    send_email(
        email_to=email_to,
        subject_template=subject,
        html_template=template_str,
        environment={
            "project_name": project_name,
            "username": email,
            "email": email_to,
            "valid_hours": settings.EMAIL_RESET_TOKEN_EXPIRE_HOURS,
            "link": link,
        },
    )


def send_new_account_email(email_to: str, username: str, password: str) -> None:
    """Sends an email using the new account template."""
    project_name: str = settings.PROJECT_NAME
    subject: str = f"{project_name} - New account for user {username}"
    with open(
        Path(settings.EMAIL_TEMPLATES_DIR) / "new_account.html", encoding="utf-8"
    ) as file:
        template_str = file.read()
    link: str = settings.SERVER_HOST
    send_email(
        email_to=email_to,
        subject_template=subject,
        html_template=template_str,
        environment={
            "project_name": project_name,
            "username": username,
            "password": password,
            "email": email_to,
            "link": link,
        },
    )


def generate_password_reset_token(email: str) -> str:
    """Creates and returns a password reset token bound to the users email."""
    delta: datetime.Date = timedelta(hours=settings.EMAIL_RESET_TOKEN_EXPIRE_HOURS)
    now = datetime.utcnow()
    expires = now + delta
    exp = expires.timestamp()
    encoded_jwt = jwt.encode(
        {"exp": exp, "nbf": now, "sub": email},
        settings.SECRET_KEY,
        algorithm="HS256",
    )
    return encoded_jwt


def verify_password_reset_token(token: str) -> Optional[str]:
    """Verifies the a password reset token is authentic."""
    try:
        decoded_token = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        return decoded_token["sub"]
    except jwt.JWTError:
        return None
