"""Contains the functions for seeding the db."""

import logging
import pickle

from sqlalchemy.orm import Session

from app import schemas
from app.core.settings import settings
from app.resources.aberoth import bosses, realms, webhooks

# make sure all SQL Alchemy models are imported (app.db.base) before initializing DB
# otherwise, SQL Alchemy might fail to initialize relationships properly
# for more details: https://github.com/tiangolo/full-stack-fastapi-postgresql/issues/28


logger: logging.Logger = logging.getLogger(__name__)


def init_db(db: Session) -> None:
    """Tables should be created with Alembic migrations
    But if you don't want to use migrations, create
    the tables un-commenting the next line
    Base.metadata.create_all(bind=engine)
    """
    # user = crud.user.get_by_email(db, email=settings.FIRST_SUPERUSER_EMAIL)
    # if not user:
    #    user_in = schemas.UserCreate(
    #        username=settings.FIRST_SUPERUSER_USERNAME,
    #        email=settings.FIRST_SUPERUSER_EMAIL,
    #        password=settings.FIRST_SUPERUSER_PASSWORD,
    #        is_superuser=True,
    #    )
    #    user = crud.user.create(db, obj_in=user_in)  # noqa: F841


def init_cache(cache):
    for realm in realms:
        for boss in bosses:
            data = cache.get(f"{realm}/{boss[0]}")
            if data is None:
                newBoss = schemas.Boss(
                    name=boss[0],
                    channel=webhooks[f"{realm}/{boss[0]}"],
                    last_killed=0,
                    min_spawn=boss[1],
                    max_spawn=boss[2],
                    camped_by="",
                )
                cache.set(f"{realm}/{boss[0]}", pickle.dumps(newBoss))

    key = cache.get("key")
    if key is None:
        cache.set("key", settings.SECRET_KEY)
    else:
        logger.info(f"Setting secret key to {str(key)}")
        settings.SECRET_KEY = str(key)
