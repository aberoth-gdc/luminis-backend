"""Returns the engine for the db."""

import logging

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app.core.settings import settings

logger: logging.Logger = logging.getLogger(__name__)

logger.info(f"Connecting to db: {settings.POSTGRES.DSN}")
engine = create_engine(str(settings.POSTGRES.DSN), pool_pre_ping=True)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
