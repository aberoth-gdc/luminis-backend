"""
"""

aroc_respawns: str = "https://discord.com/api/webhooks/1086520251325370379/EybUDTvYICrjCGGzrVnLHAbdekf5NGMIGWD11sOQHvfiPY-IR5j2MBz8tGXCdMc2rK_7"
aroc_tomb: str = "https://discord.com/api/webhooks/1086520520339636224/ty3w2JhX2PTWggzKJYxn4qywgH-hvJwBXYVJhdrlvmRRKwF4IP8ZHN56VTD1IGCFPjvU"
aroc_white_wolf: str = "https://discord.com/api/webhooks/1086520263434309703/M_UgyT2DE4TBoCuIwyOhMNpLuqL6j_TRKr_tm03B-Q9hG6KlqZ_YhMTJ1Kvj51P7a6xa"


webhooks: dict[str, str] = {
    "Arcane Yellow/Pink Rabbit": aroc_respawns,
    "Arcane Yellow/Blue Bee": aroc_respawns,
    "Arcane Yellow/Ratingar": aroc_respawns,
    "Arcane Yellow/Ourik": aroc_respawns,
    "Arcane Yellow/Master Alchemist 1": aroc_respawns,
    "Arcane Yellow/Master Alchemist 2": aroc_respawns,
    "Arcane Yellow/Shaman 1": aroc_respawns,
    "Arcane Yellow/Shaman 2": aroc_respawns,
    "Arcane Yellow/Shaman 3": aroc_respawns,
    "Arcane Yellow/Grand Shaman": aroc_respawns,
    "Arcane Yellow/White Wolf": aroc_white_wolf,
    "Arcane Yellow/Plague Rat": aroc_respawns,
    "Arcane Yellow/Forstyll": aroc_respawns,
    "Arcane Yellow/Master Thief": aroc_respawns,
    "Arcane Yellow/Minotaur": aroc_respawns,
    "Arcane Yellow/Skaldor": aroc_tomb,
    "Arcane Yellow/Crypt": aroc_tomb,
    "Arcane Yellow/Werewolf": aroc_respawns,
    "Arcane Yellow/Vampire Bats": aroc_respawns,
}


realms: list[str] = [
    # "White",
    # "Black",
    # "Green",
    # "Red",
    # "Yellow",
    # "Purple",
    # "Cyan",
    # "Blue",
    # "Arcane White",
    # "Arcane Red",
    # "Arcane Black",
    "Arcane Yellow",
]

bosses: list[tuple[str, int, int]] = [
    ("Pink Rabbit", 600, 1200),
    ("Blue Bee", 600, 1200),
    ("Ratingar", 300, 900),
    ("Ourik", 720, 1500),
    ("Shaman 1", 1080, 2160),
    ("Shaman 2", 1080, 2160),
    ("Shaman 3", 1080, 2160),
    ("Grand Shaman", 900, 3000),
    ("Master Alchemist 1", 300, 1800),
    ("Master Alchemist 2", 300, 1800),
    # ("Vampire Bats", 5, 10),
    ("Plague Rat", 900, 1800),
    ("Forstyll", 300, 4500),
    ("White Wolf", 2640, 4500),
    # ("Werewolf", 10, 10),
    ("Master Thief", 900, 1500),
    ("Minotaur", 300, 3000),
    ("Skaldor", 300, 1800),
    # ("Crypt", 3600, 3600),
]
