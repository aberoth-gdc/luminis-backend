from app.core.settings import settings

OPENAPI_DESCRIPTION: str = f"""
# Overview

Welcome to the {settings.PROJECT_NAME} API documentation. A redoc version of this can be
found at [{settings.SERVER_HOST}/redoc]({settings.SERVER_HOST}/redoc)

Please fill out this documentation with a high level overview of how to use this
API.
"""
