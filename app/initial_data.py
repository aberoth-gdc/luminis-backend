"""Initializes db with seed data."""

import logging

import redis

from app.core.settings import settings
from app.db.init_db import init_cache, init_db
from app.db.session import SessionLocal

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def init() -> None:
    """Initialize seed data for db."""
    db = SessionLocal()
    init_db(db)

    cache = redis.from_url(str(settings.REDIS.DSN))
    init_cache(cache)


def main() -> None:
    """Startup function for db seeding."""
    logger.info("Creating initial data")
    init()
    logger.info("Initial data created")


if __name__ == "__main__":
    main()
