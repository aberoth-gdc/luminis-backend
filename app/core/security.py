"""Contains utility functions for passwords and JWTs."""

from datetime import UTC, datetime, timedelta
from typing import Any

import bcrypt
from jose import jwt

from app.core.settings import settings

ALGORITHM = "HS256"


def create_access_token(subject: Any, expires_delta: timedelta | None = None) -> str:
    """Creates and return JWT access token."""
    if expires_delta:
        expire = datetime.now(UTC) + expires_delta
    else:
        expire = datetime.now(UTC) + timedelta(
            minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES
        )
    to_encode = {"exp": expire, "sub": str(subject)}
    encoded_jwt = jwt.encode(to_encode, settings.SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def verify_password(plain_password: str, hashed_password: str) -> bool:
    plain_password_e = plain_password.encode("utf-8")
    hashed_password_e = hashed_password.encode("utf-8")
    return bcrypt.checkpw(plain_password_e, hashed_password_e)


def get_password_hash(password: str) -> str:
    password_e = password.encode("utf-8")
    salt = bcrypt.gensalt(rounds=12)

    hashed_password = bcrypt.hashpw(password_e, salt)
    return hashed_password.decode()
