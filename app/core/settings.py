"""Application configuration."""

import secrets

from pydantic import AnyHttpUrl, PostgresDsn, RedisDsn, computed_field
from pydantic_settings import BaseSettings


class RedisSettings(BaseSettings):
    """Settings for connecting to Redis"""

    SCHEME: str = "redis"
    HOST: str = "redis"
    PORT: int = 6379
    PATH: str = "/"
    USER: str | None = None
    PASS: str | None = None

    @computed_field
    @property
    def DSN(self) -> RedisDsn:
        return RedisDsn.build(
            scheme=self.SCHEME,
            host=self.HOST,
            port=self.PORT,
            path=self.PATH,
            username=self.USER,
            password=self.PASS,
        )


class PostgresSettings(BaseSettings):
    """Settings for connecting to PostgreSQL"""

    HOST: str
    USER: str
    PASS: str
    DB: str

    @computed_field
    @property
    def DSN(self) -> PostgresDsn:
        return PostgresDsn.build(
            scheme="postgresql",
            username=self.USER,
            password=self.PASS,
            host=self.HOST,
            path=f"{self.DB or ''}",
        )


class SQLiteSettings(BaseSettings):
    """Settings for connecting to an SQLite"""

    @computed_field
    @property
    def DSN(self) -> str:
        return "sqlite://"


class DiscordSettings(BaseSettings):
    """Settings for connecting to Discord"""

    ID: str = "1190818877035270164"
    SECRET: str = "-7gRFSKAHlNWUuq472xz2zfDnLTbQ5J6"
    URL: AnyHttpUrl = AnyHttpUrl.build(scheme="https", host="discord.com", path="api")
    CDN: AnyHttpUrl = AnyHttpUrl.build(scheme="https", host="cdn.discordapp.com")


class Settings(BaseSettings):
    """Global settings for the backend."""

    SERVER_HOST: AnyHttpUrl = AnyHttpUrl.build(scheme="http", host="localhost")
    REDIRECT_URI: AnyHttpUrl = AnyHttpUrl.build(
        scheme="http", host="localhost", port=5173, path="login"
    )
    PROJECT_NAME: str = "Basic FastAPI"

    API_V1_STR: str = "/api/v1"
    SECRET_KEY: str = secrets.token_urlsafe(32)
    # 60 minutes * 24 hours * 8 days = 8 days
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 30
    # BACKEND_CORS_ORIGINS is a JSON-formatted list of origins
    # e.g: '["http://localhost", "http://localhost:4200", "http://localhost:3000", \
    # "http://localhost:8080", "http://local.dockertoolbox.tiangolo.com"]'
    BACKEND_CORS_ORIGINS: list[AnyHttpUrl] = [
        AnyHttpUrl.build(scheme="http", host="localhost"),
        AnyHttpUrl.build(scheme="http", host="localhost", port=5173),
    ]

    REDIS: RedisSettings
    POSTGRES: PostgresSettings

    DISCORD: DiscordSettings


settings = Settings(_env_nested_delimiter="_")
