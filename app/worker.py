"""Functions for the celery worker"""

import logging

from raven import Client

from app.core.celery_app import celery_app
from app.core.settings import settings

logger: logging.Logger = logging.getLogger(__name__)

client_sentry = Client(settings.SENTRY_DSN)


@celery_app.task(acks_late=True)
def test_celery(word: str) -> str:
    """Returns test string."""
    return f"test task return {word}"
