"""Initialization steps for the celeryworker"""

import logging

from tenacity import after_log, before_log, retry, stop_after_attempt, wait_fixed
from sqlalchemy.sql import text

from app.db.session import SessionLocal

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

MAX_TRIES = 3
WAIT_SECONDS = 5


@retry(
    stop=stop_after_attempt(MAX_TRIES),
    wait=wait_fixed(WAIT_SECONDS),
    before=before_log(logger, logging.INFO),
    after=after_log(logger, logging.WARN),
)
def init() -> None:
    """Initialize the celery worker."""
    try:
        # Try to create session to check if DB is awake
        db = SessionLocal()
        db.execute(text("SELECT 1"))
    except Exception as err:
        logger.error(err)
        raise err


def main() -> None:
    """Startup function for the celeryworker"""
    logger.info("Initializing service")
    init()
    logger.info("Service finished initializing")


if __name__ == "__main__":
    main()
