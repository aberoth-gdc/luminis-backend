"""Creates crud class for user model."""

import logging
from typing import Any

from sqlalchemy.orm import Session

from app import models, schemas
from app.core.security import get_password_hash, verify_password
from app.crud.base import CRUDBase

logger: logging.Logger = logging.getLogger(__name__)


class CRUDUser(CRUDBase[models.User, schemas.UserCreate, schemas.UserUpdate]):
    """CRUD methods for user."""

    def get_by_email(self, db: Session, *, email: str) -> models.User | None:
        """Query user from email."""
        return db.query(models.User).filter(models.User.email == email).first()

    def get_by_username(self, db: Session, *, username: str) -> models.User | None:
        """Query user from Discord ID."""
        return db.query(models.User).filter(models.User.username == username).first()

    def create(self, db: Session, *, obj_in: schemas.UserCreate) -> models.User:
        """Override create method to hash user password."""
        logger.debug(f"Creating user {obj_in.username}")
        db_obj = models.User(
            username=obj_in.username,
            email=obj_in.email,
            access_token=obj_in.access_token,
            refresh_token=obj_in.refresh_token,
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def update(
        self,
        db: Session,
        *,
        db_obj: models.User,
        obj_in: schemas.UserUpdate | dict[str, Any],
    ) -> models.User:
        """Override update method to hash user password."""
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.model_dump(exclude_unset=True, exclude_none=True)
        if "password" in update_data:
            hashed_password = get_password_hash(update_data["password"])
            del update_data["password"]
            update_data["hashed_password"] = hashed_password
        return super().update(db, db_obj=db_obj, obj_in=update_data)

    def authenticate(
        self, db: Session, *, email: str, password: str
    ) -> models.User | None:
        """Authenticate user in db."""
        _user = self.get_by_email(db, email=email)
        if not _user:
            return None
        if not verify_password(password, _user.hashed_password):
            return None
        return _user

    def is_active(self, _user: models.User) -> bool:
        """Returns true if the user is active."""
        return _user.is_active

    def is_superuser(self, _user: models.User) -> bool:
        """Returns true if the user is a super user."""
        return _user.is_superuser


user = CRUDUser(models.User)
