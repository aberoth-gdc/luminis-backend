"""Describes the user db table."""

from sqlalchemy.orm import Mapped, mapped_column

from app.db.base import Base


class User(Base):
    """Basic user for logging in."""

    id: Mapped[int] = mapped_column(primary_key=True, index=True)
    email: Mapped[str] = mapped_column(unique=True, index=True, nullable=False)
    username: Mapped[str] = mapped_column(unique=True, index=True, nullable=False)

    access_token: Mapped[str] = mapped_column(nullable=False)
    refresh_token: Mapped[str] = mapped_column(nullable=False)
