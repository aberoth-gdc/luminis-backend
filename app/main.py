"""Initializes and returns fastapi app."""

import logging

from fastapi import FastAPI
from fastapi.exception_handlers import request_validation_exception_handler
from fastapi.exceptions import RequestValidationError
from prometheus_client import make_asgi_app
from starlette.middleware.cors import CORSMiddleware

from app.api.v1.api import router
from app.core.settings import settings
from app.resources.strings import OPENAPI_DESCRIPTION

logging.basicConfig(level=logging.DEBUG)
logger: logging.Logger = logging.getLogger(__name__)

app = FastAPI(
    title=settings.PROJECT_NAME,
    version="0.1.0",
    openapi_url=f"{settings.API_V1_STR}/openapi.json",
    description=OPENAPI_DESCRIPTION,
)


# https://prometheus.github.io/client_python/multiprocess/
# Using multiprocess collector for registry
def make_metrics_app():
    # registry = CollectorRegistry()
    # multiprocess.MultiProcessCollector(registry)
    # return make_asgi_app(registry=registry)
    return make_asgi_app()


# Add prometheus asgi middleware to route /metrics requests
app.mount("/metrics", make_metrics_app())

# Set all CORS enabled origins
if settings.BACKEND_CORS_ORIGINS:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request, exc):
    """
    Extra logging for server side.
    """
    logger.debug(str(exc))
    logger.debug(await request.json())
    return await request_validation_exception_handler(request, exc)


app.include_router(router, prefix=settings.API_V1_STR)
