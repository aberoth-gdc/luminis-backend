#!/usr/bin/env bash
#vi: ft=bash

# In functions, use $@ for all extra flags, or $1, $2, $3 for specific args
set -o errexit

RUN="pipenv run"
DOCKER="sudo docker"
PYTHON="${RUN} python"
COMPOSE="${DOCKER} compose"

PACKAGE="${PWD}/app"
SCRIPTS="${PWD}/scripts"

DEV_COMPOSE="${PWD}/examples/development/docker-compose.yml"

function _pre() {
	# This registers the function name as an env variable, so we don't run functions twice.
	f=$(echo "${FUNCNAME[1]}" | sed 's/://')
	declare -g "$f"=1
}

function _rerun() {
	# Check if we've already run this function
	f=$(echo -n "_${FUNCNAME[1]}" | sed 's/://')
	return ${!f}
}

function _post() {
	# Cleanup code
	set +x
}
trap _post EXIT

_check_docker() {
	_rerun || return
	_pre
   ${DOCKER} ps &>/dev/null || \
		echo "Starting docker"; \
		sudo systemctl start docker
}

function install() {
	_rerun || return
	_pre
	FLAGS="${1:---dev}"
	pipenv install ${FLAGS}
	npm i
}

function docker:up() {
	_rerun || return
	_pre
	_check_docker
	${COMPOSE} -f ${DEV_COMPOSE} --env-file .env up -d $@ --force-recreate --build --remove-orphans
	docker:logs --follow
}

function docker:down() {
	_rerun || return
	_pre
	${COMPOSE} -f ${DEV_COMPOSE} --env-file .env down $@
}

function docker:logs() {
	_rerun || return
	_pre
	#docker:up
	${COMPOSE} -f ${DEV_COMPOSE} --env-file .env logs backend $@
}

function test:format() {
	${RUN} ruff format ${PACKAGE}
}

function test:lint() {
	${RUN} ruff check ${PACKAGE}
}

function test:unit() {
	TESTS="${1:-tests/unit}"
	${RUN} coverage run --branch --source ${PACKAGE} -m pytest ${TESTS}; \
	${RUN} coverage report
}

function test:api() {
	eval up
	#${PYTHON} ${PWD}/scripts/gen_openapi.py | npx openapi-linter lint
	# sed stuff kills colors
	npx openapi-linter lint http://localhost/api/v1/openapi.json | sed -r "s/[[:cntrl:]]\[[0-9]{1,3}m//g"
}

function test:static() {
	${RUN} mypy ${PACKAGE}
}

function test() {
	if [[ ! -n $@ ]]; then
		test:lint
		test:static
		test:unit
		test:api
	else
		test:$1 $2
	fi
}

function db:cli() {
	up
	${COMPOSE} -f ${DEV_COMPOSE} --env-file .env exec db psql --user admin fastapi_dev
}

function db:upgrade() {
	up
	${PYTHON} -m alembic upgrade head
}

function help {
  printf "%s <task> [args]\n\nTasks:\n" "${0}"

  compgen -A function | grep -v "^_" | cat -n

  #printf "\nExtended help:\n  Each task has comments for general usage\n"
}

# This allows us to use aliases in a bash script.
shopt -s expand_aliases

alias unit="test:unit"
alias static="test:static"
alias lint="test:lint"
alias format="test:format"

alias psql="db:cli"
alias upgrade="db:upgrade"

alias up="docker:up"
alias down="docker:down"
alias logs="docker:logs"

[[ ${VERBOSE} ]] && set -x

# This idea is heavily inspired by: https://github.com/adriancooney/Taskfile
TIMEFORMAT=$'\nTask completed in %3lR'
time eval "${@:-help}"
